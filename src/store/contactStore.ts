// create a store mobx
import { observable, action, computed, makeAutoObservable } from 'mobx';
import { contacts } from '../constant/contact';
import { contact } from '../types/contact';
import { Status } from '../types/status';

class store {
    constructor() {
        makeAutoObservable(this);
}
@observable contacts: contact[] = contacts;
@observable contact: contact = {
    name: '',
    phone: '',
    image: '',
    status: Status.unSubWiddy,
    nameWiddy: '',
};
@observable searchText: string ='';

@computed get getContacts() {
    return this.contacts.filter((contact) => contact.name.toLowerCase().includes(this.searchText.toLowerCase()));
}

@computed get getContactsNotFriend() {
    let result = this.contacts.filter((contact) => contact.status === Status.notFriend || contact.status === Status.isRequestFriend);
    return result.filter((contact) => contact.name.toLowerCase().includes(this.searchText.toLowerCase()));
}

@computed get getContactsUnSubWiddy() {
    let result = this.contacts.filter((contact) => contact.status === Status.unSubWiddy || contact.status === Status.inviteSubWiddy);
    return result.filter((contact) => contact.name.toLowerCase().includes(this.searchText.toLowerCase()));
}

@action inviteSubWiddy = (contact: contact) => {
    this.contacts = this.contacts.map((item) => {
        if (item.phone === contact.phone) {
           item.status = Status.inviteSubWiddy;
        }
        return item;
    });
};

@action
requestFriend = (contact: contact) => {
    this.contacts = this.contacts.map((item) => {
        if (item.phone === contact.phone) {
            item.status = Status.isRequestFriend;
        }
        return item;
    });
};

@action
setSearchText = (text: string) => {
    this.searchText = text;
};
}



const contactStore = new store();
export default contactStore;


