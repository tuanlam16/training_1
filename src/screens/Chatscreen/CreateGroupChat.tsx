import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useMemo, useRef} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  Modal,
} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import {Header} from '../../components/header';
import {ImagePicker} from '../../components/imagePicker';
import {contacts} from '../../constant/contact';
import {privacyList} from '../../constant/privacy';
import {contact} from '../../types/contact';
import {groupChat, privacy} from '../../types/groupChat';
import {height, width} from '../../untills/dimensions';
import {CheckBox} from './components/checkBox';
import {ItemImage} from './components/itemImage';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

export const CreateGroupChatScreen = ({navigation}: any) => {
  const [listContact, setListContact] = React.useState(contacts);
  const [listSelected, setListSelected] = React.useState<contact[]>([]);
  const [showModal, setShowModal] = React.useState<boolean>(false);
  const [privacySelected, setPrivacySelected] = React.useState<privacy>(privacyList[0],);
  const [image, setImage] = React.useState<string>('');
  const [groupName, setGroupName] = React.useState<string>('');
  const [groupDescription, setGroupDescription] = React.useState<string>('');
  const [showModalPrivacy, setShowModalPrivacy] =
    React.useState<boolean>(false);
  const flatListRef = useRef<FlatList>(null);

  const handleCheck = (item: contact) => {
    const index = listSelected.findIndex(i => i.phone === item.phone);
    if (index === -1) {
      setListSelected([...listSelected, item]);
    } else {
      setListSelected(listSelected.filter(i => i.phone !== item.phone));
    }
    const indexContact = contacts.findIndex(i => i.phone === item.phone);
    contacts[indexContact].checked = !contacts[indexContact].checked;
  };
  const onCloseModal = () => {
    setShowModal(false);
  };

  const onSearch = (text: string) => {
    setListContact(
      contacts.filter(item => item.name.toLowerCase().includes(text.toLowerCase())),
    );
  };

  const onAddImage = (image: string) => {
    setImage(image);
    setShowModal(false);
  };

  const onSelectPrivacy = (item: privacy) => {
    setPrivacySelected(item);
    setShowModalPrivacy(false);
  };

  const onAddName = (name: string) => {
    setGroupName(name);
  };

  const onAddDescription = (description: string) => {
    setGroupDescription(description);
  };

  const onDone = async () => {
    const id = uuidv4();
    const groupChat: groupChat = {
      id: id,
      name: groupName,
      description: groupDescription,
      image: image,
      privacy: privacySelected,
      members: listSelected,
    };
    
    const groupChats = await AsyncStorage.getItem('GROUPCHATS');
    if (groupChats) {
      const groupChatsList = JSON.parse(groupChats);
      groupChatsList.push(groupChat);
      await AsyncStorage.setItem('GROUPCHATS', JSON.stringify(groupChatsList));
    } else {
      await AsyncStorage.setItem('GROUPCHATS', JSON.stringify([groupChat]));
    }
    contacts.forEach(item => {
      item.checked = false;
    });
    navigation.goBack();

    // //clear async storage
    // await AsyncStorage.clear();
  };

  const RenderModalPrivacy =() => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={showModalPrivacy}
        onRequestClose={() => {
          setShowModalPrivacy(false);
        }}>
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={styles.modalHeader}>
              <Text style={styles.modalTitle}>Chọn quyền riêng tư</Text>
              <TouchableOpacity onPress={() => setShowModalPrivacy(false)}>
                <Text style={styles.modalClose}>X</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.modalContent}>
              <FlatList
                data={privacyList}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => onSelectPrivacy(item)}
                    style={styles.modalItem}>
                    <Image source={item.icon} style={styles.modalIcon} />
                    <View style={styles.modalItemContent}>
                      <Text style={styles.privacytitle}>{item.value}</Text>
                      <Text style={styles.privacysubtitle}>{item.description}</Text>
                    </View>
                  </TouchableOpacity>
                )}
                keyExtractor={(index) => index.toString()}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  const RenderAvatar = useMemo(() => {
    return (
      <><View style={styles.imagegroup}>
        <TouchableOpacity
          style={styles.wrapper}
          onPress={() => {
            setShowModal(true);
          } }>
          {image ? (
            <Image source={{ uri: image }} style={styles.wrapper} />
          ) : (
            <Image
              style={styles.image}
              source={require('../../assets/icons/addImage.png')} />
          )}
        </TouchableOpacity>
      </View><Text style={styles.text}>Ảnh đại diện nhóm</Text></>
    );
  }, [image]);
  
  const RenderPrivacy = useMemo(() => {
    return (
      
      <View style={styles.privacy}>
        <View style={styles.wrappPrivacy}>
          <View style={styles.icon}>
            <Image
              style={styles.iconimage}
              source={require('../../assets/icons/world.png')}
            />
          </View>
          <TouchableOpacity
            style={styles.privacytext}
            onPress={() => {
              setShowModalPrivacy(true);
            }}>
            <Text style={styles.privacytitle}>{privacySelected.value}</Text>
            <Text style={styles.privacysubtitle}>
              {privacySelected.description}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.arrow}>
          <TouchableOpacity>
            <Image
              style={styles.arrowimage}
              source={require('../../assets/icons/back.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }, [privacySelected]);

  const RenderDescription = useMemo(() => {
    return (
      
      <><View style={styles.privacy}>
        <View style={styles.wrappPrivacy}>
          <View style={styles.icon}>
            <Image
              style={styles.iconimage}
              source={require('../../assets/icons/addRound.png')} />
          </View>
          <View style={styles.privacytext}>
            <Text style={styles.privacytitle}>Mô tả</Text>
          </View>
        </View>
        <View style={styles.arrow}></View>
      </View><View style={[styles.input, { height: 100, justifyContent: 'flex-start' }]}>
          <TextInput
            multiline
            style={styles.textinput}
            placeholder="Giới thiệu ngắn gọn về mục đích hoạt động của nhóm"
            onChangeText={text => onAddDescription(text)} />
        </View></>
    );
  }, [groupDescription]);

  const RenderListSelected = useMemo(() => {
    return (
      <><Text style={[styles.text, { textAlign: 'left' }]}>
        Chọn thành viên vào nhóm
      </Text><View style={{ flex: 1, marginTop: 10 }}>
          <FlatList
            data={listSelected}
            ref={flatListRef}
            renderItem={({ item }) => (
              <ItemImage contact={item} onRemove={handleCheck} />
            )}
            keyExtractor={(item, index) => index.toString()}
            horizontal
            showsHorizontalScrollIndicator={false}
            onContentSizeChange={() => flatListRef.current.scrollToEnd()} />
        </View></>
    );
  }, [listSelected]);

  const RenderSearch = useMemo(() => {
    return (
      
      <View style={styles.input}>
        <TextInput
          placeholder="Tìm kiếm"
          style={[styles.textinput, {paddingRight: 40}]}
          onChangeText={text => onSearch(text)}
        />
        <Image
          source={require('../../assets/icons/search.png')}
          style={styles.searchIconImage}
        />
      </View>
    );
  }, [onSearch]);


  const ListHeaderComponent =  useMemo(()=>{
    return(
      <>
        {RenderAvatar}
      <View style={styles.input}>
        <TextInput
          onChangeText={text => onAddName(text)}
          style={styles.textinput}
          placeholder="Đặt tên nhóm" />
      </View>
      {RenderPrivacy}
      {RenderDescription}
      {RenderListSelected}
      {RenderSearch}
      </>

    );
  },[onAddName, onAddDescription, onSearch, listSelected]);
  return (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        title="Tạo nhóm"
        hasBackButton
        hasRightButton
      />
      <View style={{flex:1}}>
        <FlatList
          data={listContact}
          renderItem={({item}) => (
            <CheckBox
              contact={item}
              onChecked={handleCheck}
              checked={item.checked}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={ListHeaderComponent}
          extraData={listSelected}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={<Text style={{textAlign: 'center', marginTop: 20}}>Không tìm thấy liên hệ</Text>}
        />
      </View>
      <TouchableOpacity style={styles.done} onPress={onDone}>
        <Text style={styles.donetext}>Tạo nhóm</Text>
      </TouchableOpacity>
      <ImagePicker
        showModal={showModal}
        onAddImage={onAddImage}
        onCloseModal={onCloseModal}
      />
      <RenderModalPrivacy />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width-40,
    height: height,
    marginHorizontal: 20,
    marginVertical: 20,
  },
  imagegroup: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    width: 120,
    height: 120,
    borderRadius: 30,
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 50,
    height: 50,
  },
  text: {
    marginTop: 20,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    textAlign: 'center',
  },
  input: {
    marginTop: 15,
    height: 50,
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  textinput: {
    fontSize: 16,
    color: '#000000',
  },
  privacy: {
    flexDirection: 'row',
    marginTop: 30,
    justifyContent: 'space-between',
  },
  wrappPrivacy: {
    flexDirection: 'row',
  },
  icon: {
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconimage: {
    width: 15,
    height: 15,
  },
  privacytext: {
    marginLeft: 10,
    justifyContent: 'center',
  },
  privacytitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#2e2e2e',
  },
  privacysubtitle: {
    fontSize: 14,
    color: '#2e2e2e',
    width: width - 150,
  },
  arrow: {
    marginLeft: 20,
    justifyContent: 'center',
  },
  arrowimage: {
    width: 20,
    height: 20,
    transform: [{rotate: '180deg'}],
  },
  searchIconImage: {
    width: 20,
    height: 20,
    position: 'absolute',
    right: 20,
  },
  done: {
    height: 50,
    backgroundColor: '#ffa113',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  donetext: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#ffffff',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modal: {
    width: width - 40,
    height: 220,
    backgroundColor: '#ffffff',
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  modalHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  modalTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
  },
  modalClose: {
    width: 20,
    height: 20,
  },
  modalContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
  },
  modalItem: {
    flexDirection: 'row',
  },
  modalText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 10,
  },
  modalIcon: {
    width: 20,
    height: 20,
    borderRadius: 10,
  },
  modalItemContent: {
    marginLeft: 10,
    marginBottom: 20,
  }



 
});
