import { useState } from "react";
import { View, Image, Text, StyleSheet,TouchableOpacity } from "react-native";
import { contact } from "../../../types/contact";

type CheckBoxProps = {
    contact: contact
    checked: boolean;
    onChecked?: (contact: contact) => void;
};

export const CheckBox = (props: CheckBoxProps) => {
    return (
        <View style={styles.container}>
            <View style={styles.imageWrapper}>
                <Image style = {styles.image} source={props.contact?.image||require('../../../assets/images/avt.png')} />
            </View>
            <View style={styles.titleWrapper}>
                <Text style = {styles.title}>{props.contact?.name}</Text>
            </View>
            <View >
               <TouchableOpacity 
               style={styles.border} 
               onPress={() => {
                     props.onChecked?.(props.contact);
                    }}
               >
                     {props.checked && <View style={styles.checked}></View>}
                     </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 10,
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 10,
    },
    title: {
        fontSize: 16,
        color: '#000000',
    },
    imageWrapper: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: '#f5f5f5',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    titleWrapper: {
        flex: 1,
        marginLeft: 20,
    },
    border: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ffa113',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checked: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#ffa113',
    },
});