import { View, Image, StyleSheet,TouchableOpacity } from "react-native";
import { contact } from "../../../types/contact";

type ItemImageProps = {
    contact: contact,
    onRemove?: (contact:contact) => void;
};
export const ItemImage = (prop:ItemImageProps) => {
    return (
        <View style={styles.container}>
            <Image
            style={styles.image}
            source={prop.contact?.image||require('../../../assets/images/avt.png')}
            />
            <TouchableOpacity 
            style={styles.border} 
            onPress={() => {
                prop.onRemove?.(prop.contact);
            }}
             >
               <Image style={styles.remove} source={require('../../../assets/icons/remove.png')} />
            </TouchableOpacity>

        </View>
    );
    };

const styles = StyleSheet.create({
    container: {
        width: 90,
        height: 90,
        borderRadius: 10,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginRight: 10,
        paddingTop: 10,
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 10,
    },
    border: {
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        position: 'absolute',
        right: 5,
        top: 5,
    },
    remove: {
        width: 20,
        height: 20,
    },
    });