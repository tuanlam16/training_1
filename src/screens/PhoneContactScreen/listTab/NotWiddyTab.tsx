import { View, Text, FlatList } from "react-native";
import contactStore from "../../../store/contactStore";
import { observer } from "mobx-react";
import { Avatar } from "../../../components/avatar";
import { useState } from "react";
import { Status } from "../../../types/status";
import { getButtonTitle, getDisableButton, onClickButton } from "../../../untills/helper";
import { contact } from "../../../types/contact";
export const NotWiddyTab = observer(() => {
    return (
        <View>
          <FlatList
            data={contactStore.getContactsUnSubWiddy}
            renderItem={({item}) => (
              <Avatar 
                title={item.name} 
                image={item.image}
                subTitle={"Tên Widdy: " + (item.nameWiddy===""?"Chưa có":item.nameWiddy)}
                haveButton
                buttonTitle={getButtonTitle(item)}
                disabledButton={getDisableButton(item)}
                buttonOnClick={() => onClickButton(item)}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
          />
        </View>
    );
});