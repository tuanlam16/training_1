import React from "react";
import { View, Image, TextInput, ScrollView, FlatList, Text, StyleSheet } from "react-native";
import { Avatar } from "../../components/avatar";
import { Header } from "../../components/header";
import { contacts } from "../../constant/contact";
import { TopTabNavigation } from "../../navigation/tabNavigation/topTabNavigation";
import contactStore from "../../store/contactStore";

export const PhoneContactScreen = ({navigation}:any) => {
    const onSearch = (text: string) => {
      contactStore.setSearchText(text);
    };
  
    return (
      <View style={styles.container}>
        <View style={styles.topCcontent}>
        <Header title="Danh bạ điện thoại" hasBackButton={true} navigation={navigation} />
        <View style={styles.searchBar}>
          <Image
            style={styles.searchIcon}
            source={require('../../assets/icons/search.png')}
          />
          <TextInput
            onChange={e => onSearch(e.nativeEvent.text)}
            style={styles.searchInput}
            placeholder="Tìm kiếm"
          />
        </View>
        </View>
        <TopTabNavigation />
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      width: '100%',
      height: '100%',
    },
    searchBar: {
      backgroundColor: '#fafafa',
      paddingLeft: 40,
      paddingRight: 10,
      margin: 10,
      marginBottom: 20,
      borderRadius: 10,
      height: 40,
    },
    searchInput: {
      fontSize: 14,
    },
    searchIcon: {
      width: 20,
      height: 20,
      position: 'absolute',
      left: 10,
      top: 10,
      opacity: 0.5,
    },
    topCcontent: {
      marginHorizontal: 10,
      marginTop: 20,
    },
    title: {
      fontSize: 16,
      color: 'black',
      fontWeight: '500',
      margin: 10,
    },
    devider: {
      height: 1,
      backgroundColor: '#e5e5e5',
      margin: 10,
    },
  });
  