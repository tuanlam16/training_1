import AsyncStorage from '@react-native-async-storage/async-storage';
import { useIsFocused } from '@react-navigation/native';
import React, { useEffect } from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import {Avatar} from '../../components/avatar';
import {Header} from '../../components/header';
import {contacts} from '../../constant/contact';
import { groupChat } from '../../types/groupChat';
import { bottomBarHeight } from '../../untills/dimensions';

export const HomeScreen = ({navigation}:any) => {
  const [listContact, setListContact] = React.useState(contacts);
  const [listGroupChat, setListGroupChat] = React.useState<groupChat[]>([]);

  const isFocused = useIsFocused();

  useEffect(() => {
    getListGroup();
  }, [isFocused]);

  const getListGroup = async () => {
    const list = await AsyncStorage.getItem('GROUPCHATS');
    if (list) {
      setListGroupChat(JSON.parse(list));
    }
  };

  const onSearch = (text: string) => {
    setListContact(
      contacts.filter(item =>
        item.name.toLowerCase().includes(text.toLowerCase()),
      ),
    );
  };

  function renderHeader(){

    return(
      <View>
      <View>
      <Avatar
        title="Lời mời kết bạn"
        subTitle="2 lời mời kết bạn"
        image={require('../../assets/icons/add.png')}
      />
      <Avatar
        title="Danh bạ điện thoại"
        subTitle="Các liên hệ có dùng Widdy"
        image={require('../../assets/icons/contact.png')}
        onClick={() => navigation.navigate('PhoneContact')}
      />
      <Avatar
        title="Tạo nhóm"
        subTitle="Tạo nhóm mới"
        image={require('../../assets/icons/group.png')}
        onClick={() => navigation.navigate('CreateGroupChat')}
      />
    </View>
    <View style={styles.devider} />
    <Text style={styles.title}>Nhóm</Text>
    <View style ={{flex:1}}>
      <FlatList
        data={listGroupChat}
        renderItem={({item}) => (
          <Avatar
            title={item.name}
            subTitle={item.privacy.value}
            image={item.image}
            uri={item.image}
          />
        )}
        keyExtractor={item => item.id}
      />

    </View>
    <View style={styles.devider} />
    <Text style={styles.title}>Tất cả ({listContact.length})</Text>
    </View>
    )
  }

  return (
    <View style={styles.container}>
      <Header navigation={navigation} title="Bạn bè của tôi" hasBackButton={true} />
      <View style={styles.searchBar}>
        <Image
          style={styles.searchIcon}
          source={require('../../assets/icons/search.png')}
        />
        <TextInput
          onChange={e => onSearch(e.nativeEvent.text)}
          style={styles.searchInput}
          placeholder="Tìm kiếm"
        />
      </View>
        <View style={{flex:1}}>
          <FlatList style={styles.flatList}
            data={listContact}
            renderItem={({item}) => (
              <Avatar title={item.name} image={item.image} />
            )}
            keyExtractor={(item, index) => index.toString()}
            ListHeaderComponent={renderHeader}
            showsVerticalScrollIndicator={false}
          />
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    margin: 10,
    marginTop: 20,
  },
  searchBar: {
    backgroundColor: '#fafafa',
    paddingLeft: 40,
    paddingRight: 10,
    margin: 10,
    marginBottom: 20,
    borderRadius: 10,
    height: 40,
  },
  searchInput: {
    fontSize: 14,
  },
  searchIcon: {
    width: 20,
    height: 20,
    position: 'absolute',
    left: 10,
    top: 10,
    opacity: 0.5,
  },
  topCcontent: {},
  title: {
    fontSize: 16,
    color: 'black',
    fontWeight: '500',
    margin: 10,
  },
  devider: {
    height: 1,
    backgroundColor: '#e5e5e5',
    margin: 10,
  },
  flatList: {
    marginBottom: bottomBarHeight
  }
});
