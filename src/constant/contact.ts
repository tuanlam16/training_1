import { contact } from "../types/contact";
import { Status } from "../types/status";
export const contacts: contact[] = [
    {
        name: 'Phan Anh',
        image: require('../assets/images/p1.jpg'),
        phone: '0123456780',
        status: Status.isFriend,
        nameWiddy: 'Phan Anh',
    },
    {
        name: 'Dương Tử',
        image: "",
        phone: '0123456781',
        status: Status.unSubWiddy,
        nameWiddy: '',

    },
    {
        name: 'Thanh Duy',
        image: require('../assets/images/p2.jpg'),
        phone: '0123456782',
        status: Status.notFriend,
        nameWiddy: 'Thanh Duy',
    },
    {
        name: 'Trường Giang',
        image: require('../assets/images/p3.jpg'),
        phone: '0123456783',
        status: Status.notFriend,
        nameWiddy: 'Tràng Giương',
    },
    {
        name: 'Hoài Linh',
        image: require('../assets/images/p4.jpg'),
        phone: '0123456784',
        status: Status.notFriend,
        nameWiddy: 'Hoài Linh',
    },
    {
        name: 'Lộ Tư',
        image: "",
        phone: '0123456785',
        status: Status.unSubWiddy,
        nameWiddy: '',
    },
    {
        name: 'An An',
        image: require('../assets/images/p5.jpg'),
        phone: '0123456786',
        status: Status.notFriend,
        nameWiddy: 'An An',
    },
    {
        name: 'Bùi Hoàng',
        image: require('../assets/images/p6.jpg'),
        phone: '0123456787',
        status: Status.notFriend,
        nameWiddy: 'Bùi Hoàng',
    },
    {
        name: 'Lê Lâm',
        image: require('../assets/images/p7.jpg'),
        phone: '0123456788',
        status: Status.notFriend,
        nameWiddy: 'Lê Lâm',
    },
    {
        name: 'Lisa',
        image: require('../assets/images/p8.png'),
        phone: '0123456789',
        status: Status.notFriend,
        nameWiddy: 'Lisa',
    },
    {
        name: 'Triệu Mẫn',
        image: '',
        phone: '0123456780',
        status: Status.unSubWiddy,
        nameWiddy: '',
    }
];