export const privacyList = [
    {
        type: "public",
        value: "Công khai",
        description: "Tất cả mọi người có thể tìm kiếm và tham gia vào nhóm",
        icon: require("../assets/icons/world.png"),
    },
    {
        type: "private",
        value: "Riêng tư",
        description: "Chỉ có thành viên được mời mới có thể tham gia vào nhóm",
        icon: require("../assets/icons/private.png"),
    }
]