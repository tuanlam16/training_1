import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';

type HeaderProps = {
  title: string;
  hasBackButton?: boolean;
  navigation: any;
  hasRightButton?: boolean;
  onCLickRightButton?: () => void;
};

export const Header = (props: HeaderProps) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
      {props.hasBackButton && (
        <TouchableOpacity
        onPress={() =>props.navigation.goBack()}
        >
          <Image style = {styles.back} source={require('../assets/icons/back.png')} />
        </TouchableOpacity>
      )}
      <View>
        <Text style={styles.title}>{props.title}</Text>
      </View>
      </View>
      {props.hasRightButton && (
        <TouchableOpacity onPress={props.onCLickRightButton}>
          <Image
            style={styles.rightButton}
            source={require('../assets/icons/more.png')}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};
//create styles
const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    marginLeft: 10,
    marginBottom: 20,
    justifyContent: 'space-between',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    color: "#252b31",
    lineHeight: 24,
    marginLeft: 20,
  },
    back: {
    width: 20,
    height: 20,
    },
    rightButton: {
    width: 20,
    height: 20,
    marginRight: 20,
  },
  header: {
    flexDirection: 'row',
  },
});
