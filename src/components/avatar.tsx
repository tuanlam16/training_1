import { useCallback } from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {colorList} from '../constant/colorList';

type props = {
  image: string;
  title: string;
  subTitle?: string;
  onClick?: () => void;
  haveButton?: boolean;
  buttonTitle?: string;
  buttonOnClick?: () => void;
  disabledButton?: boolean;
  uri?: string;
};
export const Avatar = (props: props) => {
    const color = colorList[Math.floor(Math.random() * colorList.length)];
      const renderAvatar = useCallback(
        () => {
          return (
            <TouchableOpacity onPress={props.onClick} style={styles.leftContent}>
        <View style={[styles.avatar, {backgroundColor: color}]}>
          {props.image ? (
            <Image
              style={props.image=="" ||(!props.haveButton&&props.subTitle&&!props.uri) ? styles.image2 : styles.image}
              source={props.uri ? {uri: props.uri} : props.image}
            />
          ) : (
            <Text style={styles.text}>{props.title[0]}</Text>
          )}
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>{props.title}</Text>
          {props.subTitle && (
            <Text style={styles.subTitle}>{props.subTitle}</Text>
          )}
        </View>
      </TouchableOpacity>
          );
        },
        [props.title, props.image, props.subTitle],
      )
      


  return (
    <View style = {styles.container}>
      {renderAvatar()}
      {props.haveButton && (
        <TouchableOpacity
          onPress={props.buttonOnClick}
          style={[styles.buttonContainer, props.disabledButton?styles.buttonDisabled:null]}
          disabled={props.disabledButton}>
          <Text style={props.disabledButton?styles.buttonTitleDisable:styles.buttonTitle}>{props.buttonTitle}</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
  },
  leftContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  image2: {
    width: 20,
    height: 20,
  },
  content: {
    marginLeft: 10,
  },
  title: {
    fontSize: 16,
    color: 'black',
  },
  subTitle: {
    fontSize: 14,
    color: 'black',
    opacity: 0.5,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  text: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  buttonContainer: {
    justifyContent: 'center',
    borderRadius: 10,
    alignSelf: 'flex-end',
    marginRight: 10,
    borderWidth: 1,
    borderColor: '#ffa113',
    width: 100,
    alignItems: 'center',
    height: 35,
    paddingHorizontal: 10,
  },
  buttonTitle: {
    color: '#ffa113',
    fontSize: 16,
  },
  buttonDisabled: {
    backgroundColor: '#f5f5f5',
    borderColor: '#f5f5f5',
  },
  buttonTitleDisable: {
    color: 'black',
    fontSize: 16,
  },
});
