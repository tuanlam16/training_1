import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { observer } from "mobx-react";
import { AllContactTab } from "../../screens/PhoneContactScreen/listTab/AllContactTab";
import { NotFriendTab } from "../../screens/PhoneContactScreen/listTab/NotFriendTab";
import { NotWiddyTab } from "../../screens/PhoneContactScreen/listTab/NotWiddyTab";
import contactStore from "../../store/contactStore";
import { width } from "../../untills/dimensions";

const Tab = createMaterialTopTabNavigator();

export const TopTabNavigation = observer(() => {
    return (
            <Tab.Navigator
                tabBarOptions={{
                    indicatorStyle: {
                        backgroundColor: "#000",
                        height: 2,
                        width: 0.4,
                    },
                    labelStyle: {
                        fontSize: 16,
                        width: 'auto',
                        textTransform: 'none',
                       
                    },
                    style: {
                        backgroundColor: "#fff",
                        elevation: 0,
                        marginBottom: 20,
                    },
                    tabStyle: {
                        width: 'auto',
                    },
                    scrollEnabled: true,
                }}
                sceneContainerStyle={{
                    backgroundColor: "#fff",
                    paddingHorizontal: 10,
                }}
            >
                <Tab.Screen name={"Tất cả ("+contactStore.getContacts.length+")"} component={AllContactTab}/>
                <Tab.Screen name={"Chưa là bạn ("+contactStore.getContactsNotFriend.length+")"} component={NotFriendTab} />
                <Tab.Screen name={"Chưa đăng ký Widdy ("+contactStore.getContactsUnSubWiddy.length+")"} component={NotWiddyTab} />
            </Tab.Navigator>
    );
});
