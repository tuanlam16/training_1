import { createStackNavigator } from '@react-navigation/stack';
import { CreateGroupChatScreen } from '../screens/Chatscreen/CreateGroupChat';
import { HomeScreen } from '../screens/HomeScreen/HomeScreen';
import { PhoneContactScreen } from '../screens/PhoneContactScreen/PhoneContactScreen';

const Stack = createStackNavigator();

export const AppStack =() => {
  return (
    <Stack.Navigator
    screenOptions={
      {
        headerShown: false,
        cardStyle: { backgroundColor: 'white' },
      }
    }
    >
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="PhoneContact" component={PhoneContactScreen} />
        <Stack.Screen name="CreateGroupChat" component={CreateGroupChatScreen} />
    </Stack.Navigator>
  );
}