import React from "react";
import { AppStack } from "./appStack";
import { NavigationContainer } from "@react-navigation/native";

const MainNav = () => (
    <NavigationContainer>
        <AppStack />
    </NavigationContainer>
);
    
 export default MainNav;
    