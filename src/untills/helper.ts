import contactStore from "../store/contactStore";
import { Status } from "../types/status";

  export const getButtonTitle = (item: any) => {
        switch (item.status) {
            case Status.inviteSubWiddy:
                return "Đã mời";
            case Status.unSubWiddy:
                return "Gửi lời mời";
            case Status.notFriend:
                return "Kết bạn";
            case Status.isRequestFriend:
                return "Đã gửi";
            case Status.isFriend:
                return "Đã là bạn";
            default:
                return "Gửi lời mời";
        }
    };

    export const getDisableButton = (item: any) => {
        if(item.status === Status.inviteSubWiddy || item.status === Status.isRequestFriend || item.status === Status.isFriend) {
            return true;
        }
        return false;
    }

    export const onClickButton = (item: any) => {
        if(item.status === Status.unSubWiddy) {
            contactStore.inviteSubWiddy(item);
        }
        if(item.status === Status.notFriend) {
            contactStore.requestFriend(item);
        }
    }