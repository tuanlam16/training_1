import {contact} from "./contact";
export type groupChat = {
    id: string;
    name: string;
    image: string;
    members: contact[];
    privacy: privacy;
    description: string;
};
export type privacy = {
    type: string;
    value: string;
    description: string;
}
