import { Status } from "./status";

export type contact = {
    checked: boolean ;
    name: string;
    phone: string;
    image: string;
    status: Status;
    nameWiddy: string;
};
